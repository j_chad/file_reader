# File Reader
Reads a variable number of text files (up tp 10), returning a llst containing a user controllable amount of the files content.

## Version
* 1.0.0
* September 2018

## Dependencies

### Python
* Python 3.6.5

#### Author
* James Chadwick, 2018
